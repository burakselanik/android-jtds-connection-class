package com.adinIT.project_name;

import android.os.StrictMode;
import android.util.Log;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionHelper {

    String IP,DB,USERNAME,PASSWORD;

    public Connection connectionClass(){
        IP = "";
        DB = "";
        USERNAME = "";
        PASSWORD = "";

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        java.sql.Connection connection = null;
        String ConnectionURL = null;

        try{
            Class.forName("net.sourceforge.jtds.jdbc.Driver");
            ConnectionURL = "jdbc:jtds:sqlserver://" + IP + ";databaseName="+DB+";user="+USERNAME+";password="+PASSWORD+";";
            connection = DriverManager.getConnection(ConnectionURL);
        }
        catch (SQLException se){
            Log.e("SQL ERR: ",se.getMessage());
        }
        catch (ClassCastException e){
            Log.e("Class ERR: ",e.getMessage());
        }
        catch (Exception e){
            Log.e("System ERR: ",e.getMessage());
        }
        return connection;
    }
}
